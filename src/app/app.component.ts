import { Component } from '@angular/core';
import { HelloComponent } from './hello/hello.component';
import { RecordsService } from './records.service';

interface myData{
  obj: Object
}

// --1
function de_log(target, name, descriptor) {
  // console.log(target, name, descriptor)
  const original = descriptor.value
  /*descriptor.value = function() {
    console.log("test1: This function was hacked!")
  }*/
  // original()
  descriptor.value = function(...args) {
    console.log("Arguments ", args, "ค่าที่ส่งเข้าพังก์ชั่น")
    const result = original.apply(this, args)
    console.log("ค่าที่ได้จากฟังก์ชั่น คือ ", result)
    return result
  }

  return descriptor
}

function log(className){
  console.log(className)
  return (...args)  => {
    console.log("Arguments(อาร์กิวเมนท์) class's constructor คือ ", args)
    return new className(...args)
  }
}

// @log
/*class myExapleClass{
  constructor(arg1, arg2){
    console.log("(ข)constructor fired")
  }
}

const myClass = new myExapleClass(5, 10)*/


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  // title = 'app';
  myVariable = 'app'
  myDisabledValue = false

  text = 'app'
  updateValue (e) {
    this.text = e.target.value
    console.log(e.target.value)
  }
  text2 = 'app'

  callMyFunction() {
    this.myDisabledValue = !this.myDisabledValue
    console.log("Funcyion called!")
  }

  //--
  /*constructor() {
    console.log("ค่าที่ return กลับมา", this.aSimpleMethod(5, 2))
  }*/

  /*aSimpleMethod(a, b) {
    // console.log("Hello console!")
    return a*b
  }*/

  /*records = [
    {
      name: 'Thanakorn',
      online: true
    },
    {
      name: 'Tepsuwun',
      online: false
    },
    {
      name: 'Fame',
      online: true
    },
    {
      name: 'Thanakorn',
      online: true
    },
    {
      name: 'Tepsuwun',
      online: false
    },
    {
      name: 'Fame',
      online: true
    },
    {
      name: 'Thanakorn',
      online: true
    },
    {
      name: 'Tepsuwun',
      online: false
    },
    {
      name: 'Fame',
      online: true
    }
  ]*/
  records = []

  constructor(private myFirstService : RecordsService){

  }

  ngOnInit() {
    this.myFirstService.getData().subscribe(data => {
      this.records = data.obj
    })
  }
}
