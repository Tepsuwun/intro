import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


interface myData {
  success: boolean,
  message: string
}
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private LoggedInStatus = false

  constructor(private http: HttpClient) { }

  setLoggedIn(value: boolean){
    this.LoggedInStatus = value
  }

  get isLoggedIn(){
    return this.LoggedInStatus
  }

  getUserDetails(username, password){
    // post these details to API server return user info if correct
    return this.http.post<myData>('http://localhost:1234/api/auth.php', {
      username,
      password
    })
        /*.subscribe(data => {
      console.log(data, " is what we got from the server")
    })*/
  }

    testDatabaseDetails(username, password){
        // post these details to API server return user info if correct
        return this.http.post('http://localhost:1234/api/show.php', {
            username,
            password
        }).subscribe(data => {

            console.log(data, " is what we got from the server")
        })
    }
}
