import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.css']
})
export class HelloComponent implements OnInit {

  myvar = "Thanakorn"
  bitwiseOR = 2 | 5 // 010 101
  i = 0
  someVar = Math.random()

  constructor() {

  }

  doSomeHeavyTask(){
    console.log(`Called ${this.i++} times`)
  }

  ngOnInit() {
    setInterval (()=> {
      this.someVar = Math.random()
    }, 1000)
  }

}
