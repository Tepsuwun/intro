import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private Auth: AuthService, 
              private router: Router) { }

  ngOnInit() {
  }

  loginUser(event){
    event.preventDefault()
    const target = event.target
    const username = target.querySelector('#username').value
    const password = target.querySelector('#password').value

    this.Auth.getUserDetails(username, password).subscribe(data => {
        if(data.success){
            this.router.navigate(['admin'])
            this.Auth.setLoggedIn(true)
            localStorage.clear();
            localStorage.setItem('username', username);
            localStorage.setItem('password', password);
        } else {
            window.alert(data.message)
        }
    })
    console.log(username, password)
  }

  testForm(event){
      event.preventDefault()
      const target = event.target
      const test1 = target.querySelector('#test1').value
      const test2 = target.querySelector('#test2').value
      this.Auth.testDatabaseDetails(test1, test2)
      console.log(test1, test2)
  }
}
