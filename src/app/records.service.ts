import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

interface myData{
  obj: Array<Object>
}
@Injectable()
export class RecordsService {

  constructor(private http: HttpClient) {

  }

  getData() {
    // return this.http.get<myData>('/api/file.php')
    return this.http.get<myData>('http://localhost:1234/api/file.php')
    /*.subscribe(data => {
      console.log("data = ", data.obj)
    })*/
    /*return [
      {
        name: 'Thanakorn',
        online: true
      },
      {
        name: 'Tepsuwun',
        online: false
      },
      {
        name: 'Fame',
        online: true
      },
      {
        name: 'Thanakorn',
        online: true
      },
      {
        name: 'Tepsuwun',
        online: false
      },
      {
        name: 'Fame',
        online: true
      },
      {
        name: 'Thanakorn',
        online: true
      },
      {
        name: 'Tepsuwun',
        online: false
      },
      {
        name: 'Fame',
        online: true
      }
    ]*/
  }
}
