<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 31/7/2561
 * Time: 11:39
 */

header("Access-Control-Allow-Origin: *");
header('Content-Type: application/json');
header("Access-Control-Allow-Headers: *");

$conn = mysqli_connect("localhost", "root", "", "my-project");

mysqli_query($conn, "SET NAME utf8");
mysqli_query($conn, "SET character_set_results=utf8");
mysqli_query($conn, "SET character_set_client=utf8");
mysqli_query($conn, "SET character_set_connection=utf8");